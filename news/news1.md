---
post_size: full
tag: General
category: general
date: 2021-01-11T23:00:00.000+00:00
main_image: "/uploads/2021/01/12/hero-1.png"
news_content: "As a company, we play a unique role in the communities where we live
  and work – in good times,  and in bad. This year we’ve all been physically separated.
  But we’ve pulled together and taken care of each other from a distance. We donated
  to organizations doing important work, we raised awareness of best practices, and
  we kept people entertained when times were tough. \n\nThis December, every employee
  in every office we have worldwide came together to help children and families in
  need. We prepared special holiday gifts for children and donated over 80,000 EUR
  to charities in our local communities in Cyprus, Slovenia, Spain, India, and China. "

---

As a **company,** we play a unique role in the communities where we live and work – in good times,  and in bad. This year we’ve all been physically separated. But we’ve pulled together and taken care of each other from a distance. We donated to organizations doing important work, we raised awareness of best practices, and we kept people entertained when times were tough.

This December, every employee in every office we have worldwide came together to help children and families in need. **We prepared special holiday gifts** for children and donated over 80,000 EUR to charities in our local communities in Cyprus, Slovenia, Spain, I_ndia, and China._

<div id="anchor"></div>

New Content jbkjjnjjknjknjkkjnjmnjmjk