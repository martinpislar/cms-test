// cookie - local storage helpers

// test if browser supports LocalStorage
function storageAvailable(type) {
  try {
    var storage = window[type],
      x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return false;
  }
};

//Set the cookie
function setCookie(c_name, value, expire) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expire);
  document.cookie =
    c_name +
    "=" +
    escape(value) +
    (expire == null ? "" : ";expires=" + exdate.toGMTString()) +
    ";path=/";
}

//Get the cookie content
function getCookie(c_name) {
  if (document.cookie.length > 0) {
    c_start = document.cookie.indexOf(c_name + "=");
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1;
      c_end = document.cookie.indexOf(";", c_start);
      if (c_end == -1) {
        c_end = document.cookie.length;
      }
      return unescape(document.cookie.substring(c_start, c_end));
    }
  }
  return "";
}


export {storageAvailable, setCookie, getCookie}