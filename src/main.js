// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

// firebase db for subscription
import firebase from 'firebase/app'

// import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import "~/assets/styles.scss";

import DefaultLayout from '~/layouts/Default.vue'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyBnz1XczdsNyYOTVW5IpueAIb3tYMgF8vw',
  authDomain: 'tthd-preregistration.firebaseapp.com',
  databaseURL: 'https://tthd-preregistration.firebaseio.com',
  projectId: 'tthd-preregistration',
  storageBucket: 'tthd-preregistration.appspot.com',
  messagingSenderId: '612578857114',
  appId: '1:612578857114:web:0cca95dbaa25b84f',
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
}
