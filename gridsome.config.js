// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Mythic Legends',
  siteUrl: 'https://mythic-legends.netlify.app/',
  plugins: [
    {use: 'gridsome-plugin-robots-txt'},
    {
    use: '@gridsome/plugin-google-analytics',
    options: {
      id: '255833307'
    }
  },
  {
    use: 'gridsome-plugin-gtm',
    options: {
      id: '',
      enabled: true,
      debug: true
    }
  },
  {
    use: '@gridsome/plugin-sitemap',
  },
  // read forestry filesystem
  {
    use: "@gridsome/source-filesystem",
    options: {
      // path to files to create pages from
      path: "news/*.md",
      // template name
      typeName: "Privacy",
    },
  },
  ]
}
